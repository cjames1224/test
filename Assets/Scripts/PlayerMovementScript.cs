﻿using UnityEngine;
using System.Collections;

public class PlayerMovementScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		var x = Input.GetAxis("Horizontal") * 0.1f;
		transform.Translate (x, 0, 0);
	}
}
